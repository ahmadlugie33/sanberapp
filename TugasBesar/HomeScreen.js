import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';


const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    
    this.setState({ totalPrice: this.state.totalPrice + parseInt(price) })
  }


  render() {
    console.log(data)
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai
            {/* {'\n'} */}
            {/* <Text style={styles.headerText}>{this.props.params.userName}</Text> */}
              {/* <Text style={styles.headerText}>{this.props.params.userName}</Text> */}
            </Text>

            
            <Text style={{ textAlign: 'center' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>{this.currencyFormat(this.state.totalPrice)}</Text>
            </Text>
            {/* <Text style={{ textAlign: 'right' }}>About{'\n'} */}
            <Button style={{ textAlign: 'right'}} title='About' color='blue' 
            onPress={() => this.props.navigation.navigate('About')}/>
            {/* </Text> */}
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>

        
        <FlatList
          data={data.produk}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => {
            return (
              // <TouchableOpacity onPress={() => this.updatePrice(item.harga)}>
                <ListItem updatePrice={this.updatePrice.bind(this)} data={item} />
              // </TouchableOpacity>
            )
          }}
          numColumns={2}
        />

      </View>
    )
  }
};

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  namafunction = () => {
    console.log('')
  }
  // const namafunction = 
  render() {
    const { data, updatePrice } = this.props
    // const data = this.props.data
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain' />
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
        <Button title='BELI' color='blue'
        onPress={() => updatePrice(data.harga)}
        />
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  
  itemContainer: {
    width: DEVICE.width * 0.44,
    padding: 4,
    margin: 8,
    backgroundColor: 'white',
    alignItems: 'center'
  },
  itemImage: {
    height: 100,
    width: 100
  },
  itemName: {
    fontWeight: 'bold',
    minHeight: 40
  },
  itemPrice: {
    fontSize: 18,
    color: 'blue'
  },
  itemStock: {
  },
  itemButton: {
  },
  buttonText: {
  }
})
