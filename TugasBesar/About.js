import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
        }
    }
    render() {
        return (
            <View style={styles.container}>
               
                    <Text style={styles.tittle}>About E-Cossan</Text>
                    {/* <Image source={require('./images/user.png')} style={{width:200,height:200, borderRadius:100, alignSelf:'center'}} /> */}
                    <Text style={styles.name}> Email: ahmadlugie33@gmail.com</Text>
                    <Text style={styles.name}> Gitlab: @ahmadlugie33</Text>
                    {/* <Text style={styles.name}> Google Drive: @ahmadlugie33</Text> */}
                    <Text style={styles.name}> Figma: https://www.figma.com/file/yXDnXdfYegRkR00xBCMrjl/Untitled?node-id=0%3A1</Text>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        fontFamily:'Roboto',
        color:'#003366',
        alignItems:'center',
        justifyContent:'center',
    },
    
    logo: {
        width: 375,
        height: 116,
        marginTop: 59,
    },
    tittle: {
        alignSelf: "center",
        margin: 1,
        fontSize: 24,
        lineHeight: 28,
        color: '#003366',
    },
    textForm: {
        fontSize: 16,
        lineHeight: 19,
        color: '#003366',
    },
    inputForm: {
        width: 294,
        height: 35,
        borderColor: 'gray',
        borderWidth: 1
    },
    boxButton: {
        alignItems: 'center',
        marginVertical: 15,
        alignSelf: "center",

    },
    textButton: {
        textAlign: "center",
        fontSize: 24,
        color: "white",
        padding: 5,
    },
    lightButton: {
        borderWidth: 1,
        backgroundColor: "#3ec6ff",
        width: 140,
        height: 40,
        borderRadius: 15,
        borderColor: "#3ec6ff",
    },
    darkButton: {
        borderWidth: 1,
        backgroundColor: '#003366',
        width: 140,
        height: 40,
        borderRadius: 15,
        borderColor: '#003366',
    },
    titleText: {
        fontSize: 48,
        fontWeight: 'bold',
        color: 'blue',
        textAlign: 'center',
    },
    subTitleText: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'blue',
        alignSelf: 'flex-end',
        marginBottom: 16
    },
    formContainer: {
        justifyContent: 'center'
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 16
    },
    labelText: {
        fontWeight: 'bold'
    },
    textInput: {
        width: 300,
        backgroundColor: 'white'
    },
    errorText: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 16,
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    }
});