import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import StackNavigator from './routes/StackNavigator';
import Index from './index';
import Login from './LoginScreen';

export default function App() {
  return (
    // <View style={styles.container}>
    //   {/* <Text>FBI Open up </Text> */}
    //   {/* <StackNavigator/> */}
    //   {/* <Login /> */}
      
    //   <StatusBar style="auto" />
    // </View>
    <Index/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
